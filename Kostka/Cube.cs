﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _0703Kostka
{
    class Cube
    {
        private int[] Scianki { get; set; }
        private int Rozmiar { get; set; }
        Random _randomGenerator;  //zmienna globalna - podkreślenie to dobre praktyki

        public Cube() :this(6) { }

        public Cube(int rozmiar)
        {
            Rozmiar = rozmiar;
            Scianki = new int[rozmiar];
            _randomGenerator = new Random();

            for(int index = 0; index < rozmiar; index++)
            {
                Scianki[index] = index++;
            }       
        }

        public int DajWartoscScianki(int indexScianki)
        {
            return Scianki[indexScianki];
        }

        public int RzucKostka()
        {
            return Scianki[_randomGenerator.Next(0, Rozmiar)];
        }
    }
}
