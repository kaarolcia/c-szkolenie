﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _0703Kostka
{
    class Program
    {
        static void Main(string[] args)
        {
            Cube kosc = new Cube(); // ten konstruktor korzysta z :this(6)
            Cube koscDuza = new Cube(120);
            Console.WriteLine($"Ostatnia scianka: { kosc.DajWartoscScianki(5)}");
            Console.WriteLine("----------------------------------- \n");

            Console.WriteLine($"Losowanie pierwsze: {kosc.RzucKostka()}");
            Console.WriteLine($"Losowanie drugie: {kosc.RzucKostka()}");
            Console.WriteLine($"Losowanie trzecie: {kosc.RzucKostka()}");
            Console.WriteLine($"Losowanie czwarte: {kosc.RzucKostka()}");
            Console.WriteLine("----------------------------------- \n");

            Console.WriteLine("A teraz duza kosc: ");
            Console.WriteLine($"Losowanie pierwsze: {koscDuza.RzucKostka()}");
            Console.WriteLine($"Losowanie drugie: {koscDuza.RzucKostka()}");
            Console.WriteLine($"Losowanie trzecie: {koscDuza.RzucKostka()}");
            Console.WriteLine($"Losowanie czwarte: {koscDuza.RzucKostka()}");
            Console.WriteLine($"Losowanie piate: {koscDuza.RzucKostka()}");
            Console.WriteLine($"Losowanie szoste: {koscDuza.RzucKostka()}");
            Console.WriteLine($"Losowanie siodme: {koscDuza.RzucKostka()}");
            Console.WriteLine($"Losowanie osme: {koscDuza.RzucKostka()}");
            Console.WriteLine($"Losowanie dziewiate: {koscDuza.RzucKostka()}");


            Console.ReadKey();
        }
    }
}
