﻿namespace Classes
{
    internal class Book
    {

        public string Title { get; set; }
        public string Author { get; set; }
        public int Year { get; set; }
        public bool State { get; set; }

        public Book(string Title, string Author, int Year)
        {
            this.Title = Title;
            this.Author = Author;
            this.Year = Year;
            this.State = State;
            State = true;
        }

        public string ShowBookDetails()
        {
            return string.Format("\"{0}\" by {1}, {2}", Title, Author, Year);
        }

        public string checkBookStatus()
        {
            string available;
            if (State == true)
            {
                available = "available";
            }
            else
            {
                available = "not available";
            }
            return available;
        }


    }
}