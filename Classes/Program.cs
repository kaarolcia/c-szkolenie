﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Welcome to my main method.");

            Book book = new Book("Master of C# disaster", "Staples Person", 2017);
            Console.WriteLine(book.ShowBookDetails());
            string available = book.checkBookStatus();

            Console.WriteLine("This book is " + available + ".");
            Console.WriteLine("I want to borrow this book!");

            available = book.checkBookStatus();

            Console.WriteLine("This book is now " + available + ".");
            Console.WriteLine("The End.");
            Console.ReadKey();

        }
    }
}
