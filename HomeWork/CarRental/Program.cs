﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;

namespace CarRental
{
    class Program
    {
        static void Main(string[] args)
        {
            RentACar rentACar = new RentACar();

            Client jan = new Client("jan", "smok", true, 56);
            Client stefan = new Client("sefan", "zielony", true, 33);
            Client antonina = new Client("antonina", "spiewak", false, 61);
            Client kryspin = new Client("kryspin", "oreb", true, 26);
            Client bozena = new Client("bozena", "spisz", false, 19);
            Client dawid = new Client("dawid", "kropek", true, 33);
            Client klakier = new Client("klakier", "smoła", false, 18);
            Client john = new Client("john", "siema", true, 16);

            Car audia4 = new Car("audi", "a4", "plate1", 4);
            Car mazda3 = new Car("mazda", "3", "plates2", 5);
            Car mazda5 = new Car("mazda", "5", "plate3", 7);
            Car seatleon = new Car("seat", "leon", "plates4", 5);
            Car honda = new Car("honda", "civis", "plate5", 4);
            Car skoda = new Car("skoda", "octavia", "plates6", 5);
            Car zuk = new Car("zuk", "zuk", "plate7", 3);
            Car smart = new Car("smart", "smart", "plates8", 2);

            Scooter scooter1 = new Scooter("opel", "sctr", "plates9");
            Scooter scooter2 = new Scooter("opel", "sctr2jol", "plates10");
            Scooter scooter3 = new Scooter("opel", "sctr2jol", "plates11");
            Scooter scooter4 = new Scooter("opel", "sctr2jol", "plates12");
            Scooter scooter5 = new Scooter("opel", "sctr2jol", "plates13");
            Scooter scooter6 = new Scooter("opel", "sctr2jol", "plates14");

            rentACar.AddVehicule(audia4);
            rentACar.AddVehicule(mazda3);
            rentACar.AddVehicule(mazda5);
            rentACar.AddVehicule(seatleon);
            rentACar.AddVehicule(honda);
            rentACar.AddVehicule(skoda);
            rentACar.AddVehicule(zuk);
            rentACar.AddVehicule(smart);
            rentACar.AddVehicule(scooter1);
            rentACar.AddVehicule(scooter3);
            rentACar.AddVehicule(scooter4);
            rentACar.AddVehicule(scooter5);
            rentACar.AddVehicule(scooter6);

            Console.WriteLine("-----------------------------------------------------------");

            rentACar.RentCar(audia4, antonina);
            rentACar.RentCar(scooter2, stefan);
            rentACar.RentCar(seatleon, dawid);
            rentACar.RentCar(scooter6, klakier);
            rentACar.RentCar(scooter4, john);
            rentACar.RentCar(mazda5, bozena);

            Console.WriteLine("-----------------------------------------------------------");

            rentACar.ShowAvailableVehicules();
            Console.ReadKey();
        }
    }

}
