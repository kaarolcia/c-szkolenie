﻿namespace CarRental
{
    internal class Client
    {
        public string FirstName { get; set; }
        public string Lastname { get; set; }
        public bool HasDrivingLicence { get; set; }
        public int Age { get; set; }

        public Client(string firstName, string lastName, bool hasDl, int age)
        {
            FirstName = firstName;
            Lastname = lastName;
            HasDrivingLicence = hasDl;
            Age = age;
        }
    }
}