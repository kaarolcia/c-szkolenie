﻿namespace CarRental
{
    internal class Scooter : Vehicle
    {
      //  public int Seatings { get; }

        public Scooter(string brand, string model, string plate) : base(brand, model, plate)
        {
            Seatings = 2;
        }
    }
}