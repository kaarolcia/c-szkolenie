﻿using System;

namespace CarRental
{
    //klasa: pojazdZWypozyczalni
    internal class RentalCar
    {
        public IVehicle Vehicle { get; set; }
        public bool IsAvailable { get; set; }
        public string RentingFirstName { get; set; }
        public string RentingLastName { get; set; }
        public DateTime RentDate { get; set; }

        public RentalCar(IVehicle vehicle)
        {
            Vehicle = vehicle;
        }


    }
}