﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRental
{
    interface IVehicle
    {
        int Seatings { get; set; }
        void Go();
    }
}
