﻿using System;
using System.Collections.Generic;

namespace CarRental
{
    //klasa: wypozyczalniaPojazdow
    internal class RentACar
    {
        public List<RentalCar> CarList { get; set; }

        public RentACar()
        {
            CarList = new List<RentalCar>();
        }

        public void AddVehicule(IVehicle vehicle)
        {
            RentalCar rentalVehicle = new RentalCar(vehicle);
            rentalVehicle.IsAvailable = true;
            CarList.Add(rentalVehicle);
        }

        public void RentCar(Vehicle carToRent, Client client)
        {
            if (client.HasDrivingLicence)
            {
                foreach (RentalCar car in CarList)
                {
                    if (car.IsAvailable && car.GetType() == typeof(Car))
                    {
                        car.IsAvailable = false;
                        car.RentingFirstName = client.FirstName;
                        car.RentingLastName = client.Lastname;
                        car.RentDate = DateTime.Now;
                        Console.WriteLine($"Client {client.FirstName} {client.Lastname} has rented car {carToRent.Brand} {carToRent.Model}");
                        break;
                    }
                }
            }
            else
            {
                Console.WriteLine($"Client {client.FirstName} {client.Lastname} cant rent a car because one has no driving licence.");
            }
        }

        public void RentScooter(Client client)
        {
            if (client.Age > 17)
            {
                foreach (RentalCar car in CarList)
                {
                    if (car.IsAvailable && car.GetType() == typeof(Scooter))
                    {
                        car.IsAvailable = false;
                        car.RentingFirstName = client.FirstName;
                        car.RentingLastName = client.Lastname;
                        car.RentDate = DateTime.Now;
                        Console.WriteLine($"Client {client.FirstName} {client.Lastname} has rented scooter.");
                        break;
                    }
                }
            }
            else
            {
                Console.WriteLine($"Client {client.FirstName} {client.Lastname} cannot rented scooter, is underage.");
            }
        }

        public void ShowCarWithPlate(string plate)
        {
            foreach (RentalCar car in CarList)
            {

            }
        }

        public void ShowAvailableVehicules()
        {
            foreach (RentalCar car in CarList)
            {
                if (car.IsAvailable)
                {
                    Console.WriteLine($"Car {car.Vehicle.Brand}");
                }
            }
        }
    }
}