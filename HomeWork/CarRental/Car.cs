﻿namespace CarRental
{
    internal class Car : Vehicle
    {
        //public int Seatings { get; set; }

        public Car(string brand, string model, string plate, int seatings) : base(brand, model, plate, seatings)
        {
            Seatings = seatings;
        }

        public Car() : base()
        {
        }
    }
}