﻿using System;

namespace CarRental
{
    internal class Vehicle : IVehicle
    {
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Plate { get; set; }
        public int Seatings { get; set; }

        public Vehicle(string brand, string model, string plate, int seatings)
        {
            Brand = brand;
            Model = model;
            Plate = plate;
            Seatings = seatings;
        }

        public Vehicle(string brand, string model, string plate)
        {
            Brand = brand;
            Model = model;
            Plate = plate;
        }

        public Vehicle()
        {
        }

        public void Go()
        {
            Console.WriteLine("Drive!");
        }
    }
}