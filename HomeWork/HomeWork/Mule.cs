﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork
{
   internal class Mule : Animal, ITrainAnimal
    {
        public int MaxWeight { get; set; }

        public Mule(string name, string sound) : base(name, sound)
        {
            MaxWeight = 100;
            Kind = "Mule";
        }

        public void Pull()
        {
            Console.WriteLine($"{Kind} {Name} is pulling now.");
        }



        
    }
}
