﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Farm farm = new Farm();

            Pigeon pigeon1 = new Pigeon("stefan", "gru gru");
            Cow cow1 = new Cow("mućka", "muuuu");
            Mule mule1 = new Mule("muł", "muuuu");
            Pigeon pigeon2 = new Pigeon("inek", "gru gru");
            Cow cow2 = new Cow("krufka", "muuuu");
            Mule mule2 = new Mule("mułek", "muuuu");
            Mule mul = new Mule("muuuulino", "muuu");
            Hen hen = new Hen("heniek", "kukuryku");
            farm.AddAnimalToFarm(pigeon1);
            farm.AddAnimalToFarm(cow1);
            farm.AddAnimalToFarm(mule1);
            farm.AddAnimalToFarm(pigeon2);
            farm.AddAnimalToFarm(cow2);
            farm.AddAnimalToFarm(mule2);
            farm.AddAnimalToFarm(mul);
            farm.AddAnimalToFarm(hen);

            farm.ListAllAnimalsOnFarm();
            farm.ListAllTrainAnimals();
            farm.ListAllBirds();
            farm.ListAllFlyingAnimals();
            farm.AllAnimalsMakeSound();

            Console.ReadKey();

        }
    }
}
