﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork
{
    internal interface ITrainAnimal
    {
        int MaxWeight { get; set; }
        void Pull();
    }
}
