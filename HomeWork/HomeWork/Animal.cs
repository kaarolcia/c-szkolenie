﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork
{
    internal class Animal
    {

        public string Name;
        public string Kind;
        public string Sound;

        public Animal(string name, string sound)
        {
            Name = name;
            
            Sound = sound;
        }

        public void Eats()
        {
            Console.WriteLine($"{Kind} {Name} is eating now.");
        }

        public void MakesSound()
        {
            Console.WriteLine(Sound);
        }

        public void Walks()
        {
            Console.WriteLine($"{Kind} {Name} is walking now.");
        }

    }
}
