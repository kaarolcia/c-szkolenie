﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork
{
    internal class Pigeon : Animal, IBird
    {
        public bool CanFly { get; set; }

        public Pigeon(string name, string sound) : base(name, sound)
        {
            Kind = "Pigeon";
            CanFly = true;
        }

        public void Fly()
        {
            Console.WriteLine($"{Kind} {Name} is flying now.");
        }

    }
}
