﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork
{
    internal class Horse : Animal, ITrainAnimal
    {
        public int MaxWeight { get; set; }

        public Horse(string name, string sound) : base(name, sound)
        {
            Kind = "Horse";
        }

        public void Pull()
        {
            Console.WriteLine($"{Kind} {Name} is pulling now.");
        }

        public void Gallop()
        {
            Console.WriteLine($"{Kind} {Name} is galloping now.");
        }

    }
}
