﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork
{
    internal class Hen : Animal, IBird
    {
        public bool CanFly { get; set; }

        public Hen(string name, string sound) : base(name, sound)
        {
            Kind = "Hen";
            CanFly = false;
        }

        public void Fly()
        {
            Console.WriteLine($"{Kind} {Name} is flying now.");
        }

    }
}
