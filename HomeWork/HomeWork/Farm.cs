﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork
{
    internal class Farm
    {
        public List<Animal> Animals = new List<Animal>();

        public int NumberOfAnimals => Animals.Count;

        public void AddAnimalToFarm(Animal animal)
        {
            Animals.Add(animal);
        }

        public void ListAllAnimalsOnFarm()
        {
            Console.WriteLine("Animals on the farm:");
            foreach (Animal animal in Animals)
            {
                Console.WriteLine($"{animal.Kind} {animal.Name}");
            }
        }

        public void ListAllTrainAnimals()
        {
            Console.WriteLine("Train animals on the farm:");
            foreach (Animal animal in Animals)
            {
                if (animal is ITrainAnimal)
                {
                    Console.WriteLine($"Train Animal: {animal.Kind} {animal.Name}");
                }
            }
        }

        public void ListAllBirds()
        {
            Console.WriteLine("Birds on the farm:");
            foreach (Animal animal in Animals)
            {
                if (animal is IBird)
                {
                    Console.WriteLine($"Bird: {animal.Kind} {animal.Name}");
                }
            }
        }

        public void ListAllFlyingAnimals()
        {
            string canFly = "Yes";
            Console.WriteLine("Flying animals on the farm:");
            foreach (Animal animal in Animals)
            {
                if (animal is IBird)
                {
                    IBird bird = (IBird)animal;
                    if(bird.CanFly == false)
                    {
                        canFly = "No";
                    }
                    Console.WriteLine($"Bird: {animal.Kind} {animal.Name}. Can it fly? {canFly}");
                }
            }
        }

        public void AllAnimalsMakeSound()
        {
            Console.WriteLine("Animals are making sound: ");
            foreach (Animal animal in Animals)
            {
                animal.MakesSound();
            }
        }
    }
}
