﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork
{
    internal class Cow : Animal
    {
        public Cow(string name, string sound) : base(name, sound)
        {
            Kind = "Cow";
        }
    }
}
