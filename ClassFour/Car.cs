﻿using System;

namespace ClassFour
{
    internal class Car
    {
        public int MaxNumberOfPpl;
        public Person[] ListOfPpl;
        public bool carState;
        public Person emptySeat = new Person("","",false);

        public Car(int MaxNumberOfPpl)
        {
            this.MaxNumberOfPpl = MaxNumberOfPpl;
            ListOfPpl = new Person[MaxNumberOfPpl];
            carState = false;
        }

        public void addPassenger(Person person)
        {
            for (int index = 1; index < ListOfPpl.Length; index++)
            {
                if (ListOfPpl[0] == null && person.drivingLicence.Equals(true))
                {
                    ListOfPpl[0] = person;
                    Console.WriteLine("Driver added: " + person.FirstName + " " + person.LastName);
                    break;
                }
                else 
                {
                    if (ListOfPpl[index] == null)
                    {
                        ListOfPpl[index] = person;
                        Console.WriteLine("Passenger " + person.FirstName + " " + person.LastName + " has been added to this car.");
                        break;
                    }
                }

            }

        }

        public void removePassenger(Person person)
        {
            for (int index = 0; index < ListOfPpl.Length; index++) { 
                if (ListOfPpl[index] == person)
                {
                    ListOfPpl[index] = emptySeat;
                    Console.WriteLine("Passenger " + person.FirstName + " " + person.LastName + " has been removed from this car.");
                }
            }
        }

        public void showAllPassengers()
        {
            Console.WriteLine("\n--------------------------------------");
            Console.WriteLine("Our passengers are: ");
            foreach (Person person in ListOfPpl)
            {
                if (!(person.FirstName == "" || person.LastName == "")) {
                    Console.WriteLine(person.FirstName + " " + person.LastName);
                }
            }
        }

        public void startCar()
        {
            if (carState == false && ListOfPpl[0].drivingLicence == true)
            {
                carState = true;
                Console.WriteLine("The car has started.");
            }
            else
            {
                Console.WriteLine("Car cannot be started.");
            }
        }
    }
}