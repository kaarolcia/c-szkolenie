﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassFour
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car(4);
            Person stefan = new Person("Stefan", "Jeden", false);
            Person andzej = new Person("Andzej", "Drugi", true);
            Person kryspin = new Person("Kryspin", "Trzeci", false);
            Person bolek = new Person("Bolek", "Czwarty", true);
            Person jurek = new Person("Jurek", "Piąty", false);

            car.addPassenger(stefan);
            car.addPassenger(andzej);
            car.addPassenger(kryspin);
            car.addPassenger(bolek);

            car.removePassenger(kryspin);

            car.showAllPassengers();

            car.startCar();

            Console.ReadKey();
        }
    }
}
