﻿namespace ClassFour
{
    internal class Person
    {

        public string FirstName;
        public string LastName;
        public bool drivingLicence;

        public Person()
        {
        }

        public Person(string FirstName, string LastName, bool drivingLicence)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.drivingLicence = drivingLicence;
        }
    }
}